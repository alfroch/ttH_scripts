import numpy as np
from glob import glob
import uproot3 as upt
import os
import matplotlib.pyplot as plt
import matplotlib as mtp
import awkward0 as awk


def hist_w_unc(
    a: np.ndarray,
    bins,
    normed: bool = True,
):
    """
    Computes histogram and the associated statistical uncertainty.

    Parameters
    ----------
    a : array_like
        Input data. The histogram is computed over the flattened array.
    bins: int or sequence of scalars or str
        bins parameter from np.histogram
    normed: bool
        If True (default) the calculated histogram is normalised to an integral
        of 1.

    Returns
    -------
    bin_edges : array of dtype float
        Return the bin edges (length(hist)+1)
    hist : numpy array
        The values of the histogram. If normed is true (default), returns the
        normed counts per bin
    unc : numpy array
        Statistical uncertainty per bin.
        If normed is true (default), returns the normed values.
    band : numpy array
        lower uncertainty band location: hist - unc
        If normed is true (default), returns the normed values.
    """
    arr_length = len(a)

    # Calculate the counts and the bin edges
    counts, bin_edges = np.histogram(a, bins=bins)

    unc = np.sqrt(counts) / arr_length if normed else np.sqrt(counts)
    band = counts / arr_length - unc if normed else counts - unc
    hist = counts / arr_length if normed else counts

    return bin_edges, hist, unc, band


def save_divide(
    nominator: np.ndarray,
    denominator: np.ndarray,
    default: float = 1.0,
):
    """
    Division using numpy divide function returning default value in cases where
    denoinator is 0.

    Parameters
    ----------
    nominator: array_like
        Nominator in the ratio calculation.
    denominator: array_like
        Denominator in the ratio calculation.
    default: float
        default value which is returned if denominator is 0.

    Returns
    -------
    ratio: array_like
    """
    if isinstance(nominator, (int, float)) and isinstance(denominator, (int, float)):
        output_shape = 1
    else:
        try:
            output_shape = denominator.shape
        except AttributeError:
            output_shape = nominator.shape

    ratio = np.divide(
        nominator,
        denominator,
        out=np.ones(
            output_shape,
            dtype=float,
        )
        * default,
        where=(denominator != 0),
    )
    return ratio


def hist_ratio(
    nominator: np.ndarray,
    denominator: np.ndarray,
    nominator_unc: np.ndarray,
    denominator_unc: np.ndarray,
):
    """
    This method calculates the ratio of the given bincounts and
    returns the input for a step function that plots the ratio.

    Parameters
    ----------
    nominator : array_like
        Nominator in the ratio calculation.
    denominator : array_like
        Denominator in the ratio calculation.
    nominator_unc : array_like
        Uncertainty of the nominator.
    denominator_unc : array_like
        Uncertainty of the denominator.


    Returns
    -------
    step_ratio : array_like
        Ratio returning 1 in case the denominator is 0.
    step_ratio_unc : array_like
        Stat. uncertainty of the step_ratio

    Raises
    ------
    AssertionError
        If inputs don't have the same shape.

    """
    if nominator.shape != denominator.shape:
        raise AssertionError("Nominator and denominator don't have the same legth")
    if nominator.shape != nominator_unc.shape:
        raise AssertionError("Nominator and nominator_unc don't have the same legth")
    if denominator.shape != denominator_unc.shape:
        raise (
            AssertionError("Denominator and denominator_unc don't have the same legth")
        )
    step_ratio = save_divide(nominator, denominator)
    # Add an extra bin in the beginning to have the same binning as the input
    # Otherwise, the ratio will not be exactly above each other (due to step)
    step_ratio = np.append(np.array([step_ratio[0]]), step_ratio)

    # Calculate rel uncertainties
    nominator_rel_unc = save_divide(nominator_unc, nominator, default=0)
    denominator_rel_unc = save_divide(denominator_unc, denominator, default=0)

    # Calculate rel uncertainty
    step_rel_unc = np.sqrt(nominator_rel_unc ** 2 + denominator_rel_unc ** 2)

    # Add the first value again (same reason as for the step calculation)
    step_rel_unc = np.append(np.array([step_rel_unc[0]]), step_rel_unc)

    # Calculate final uncertainty
    step_unc = step_ratio * step_rel_unc

    return step_ratio, step_unc


def applyATLASstyle(mtp):
    font_dir = os.path.abspath(__file__).replace("PyATLASstyle.py", "fonts/")
    font_dirs = [
        font_dir,
    ]

    import matplotlib.font_manager as font_manager

    font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
    for f in font_files:
        font_manager.FontManager.addfont(font_manager.fontManager, path=f)
    mtp.rcParams["font.size"] = 10
    mtp.rcParams["legend.frameon"] = False
    mtp.rcParams["legend.fontsize"] = 10
    mtp.rcParams["lines.antialiased"] = False
    mtp.rcParams["lines.linewidth"] = 2.5
    mtp.rcParams["xtick.direction"] = "in"
    mtp.rcParams["xtick.top"] = True
    mtp.rcParams["xtick.minor.visible"] = True
    mtp.rcParams["xtick.major.size"] = 10
    mtp.rcParams["xtick.minor.size"] = 5
    mtp.rcParams["ytick.direction"] = "in"
    mtp.rcParams["ytick.right"] = True
    mtp.rcParams["ytick.minor.visible"] = True
    mtp.rcParams["ytick.major.size"] = 10
    mtp.rcParams["ytick.minor.size"] = 5
    mtp.rcParams["axes.unicode_minus"] = False
    mtp.rcParams["pdf.fonttype"] = 3


def makeATLAStag(
    ax,
    fig,
    first_tag: str = "",
    second_tag: str = "",
    xmin: float = 0.04,
    ymax: float = 0.85,
    fontsize: int = 10,
):
    line_spacing = 0.6
    box0 = ax.text(
        xmin,
        ymax,
        "ATLAS",
        fontweight="bold",
        fontstyle="italic",
        verticalalignment="bottom",
        transform=ax.transAxes,
        fontsize=fontsize,
    )
    box0_ext_tr = ax.transAxes.inverted().transform(
        box0.get_window_extent(renderer=fig.canvas.get_renderer())
    )
    box1 = ax.text(
        box0_ext_tr[1][0],
        ymax,
        " ",
        verticalalignment="bottom",
        transform=ax.transAxes,
        fontsize=fontsize,
    )
    box1_ext_tr = ax.transAxes.inverted().transform(
        box1.get_window_extent(renderer=fig.canvas.get_renderer())
    )
    ax.text(
        box1_ext_tr[1][0],
        ymax,
        first_tag,
        verticalalignment="bottom",
        transform=ax.transAxes,
        fontsize=fontsize,
    )
    ax.text(
        xmin,
        ymax
        - (box0_ext_tr[1][1] - box0_ext_tr[0][1])
        * (line_spacing + len(second_tag.split("\n"))),
        second_tag,
        verticalalignment="bottom",
        transform=ax.transAxes,
        fontsize=fontsize,
    )


def plot_ratio_hist(
    df_list: list,
    variable: str,
    plot_path: str,
    label_list: str,
    plot_errors: bool = True,
    plot_type: str = "pdf",
    ApplyAtlasStyle: bool = True,
    figsize: list = None,
    nBins: int = 50,
    labelFontSize: int = 10,
    legFontSize: int = 10,
    labelpad: int = None,
    Ratio_Cut: list = None,
    xmin: float = None,
    xmax: float = None,
    ymin: float = None,
    ymax: float = None,
    UseAtlasTag: bool = True,
    AtlasTag: str = "Internal Simulation",
    SecondTag: str = "ttH Comparison",
    yAxisAtlasTag: float = 0.9,
    yAxisIncrease: float = 1.3,
    loc_legend: str = "best",
    ncol: int = 1,
    dpi: int = 400,
    title: str = "",
    ycolor: str = "black",
    verbose: bool = True,
):
    # Print variable
    if verbose:
        print(f"Plotting variable {variable}")

    # Apply the ATLAS Style with the bars on the axes
    if ApplyAtlasStyle is True:
        applyATLASstyle(mtp)

    # Define the figure with two subplots of unequal sizes
    axis_dict = {}

    if figsize is None:
        fig = plt.figure(figsize=(11.69 * 0.8, 8.27 * 0.8))

    else:
        fig = plt.figure(figsize=(figsize[0], figsize[1]))

    gs = mtp.gridspec.GridSpec(8, 1, figure=fig)
    axis_dict["left"] = {}
    axis_dict["left"]["top"] = fig.add_subplot(gs[:6, 0])
    axis_dict["left"]["ratio"] = fig.add_subplot(
        gs[6:, 0], sharex=axis_dict["left"]["top"]
    )

    # Get binning for the plot
    try:
        _, Binning = np.histogram(
            df_list[0][variable],
            bins=nBins,
        )

    except KeyError:
        try:
            _, Binning = np.histogram(
                df_list[1][variable],
                bins=nBins,
            )

        except ValueError:
            return

    # Init bincout and unc dict for ratio calculation
    bincounts = {}
    bincounts_unc = {}

    linestyles = ["solid", "dashed", "dotted", "dashdot"]

    for i, (df_results, linestyle, label) in enumerate(
        zip(
            df_list,
            linestyles,
            label_list,
        )
    ):
        try:
            bins, weights, unc, band = hist_w_unc(
                a=df_results[variable],
                bins=Binning,
            )

        except KeyError:
            continue

        hist_counts, _, _ = axis_dict["left"]["top"].hist(
            x=bins[:-1],
            bins=bins,
            weights=weights,
            histtype="step",
            linewidth=2.0,
            linestyle=linestyle,
            color=f"C{i}",
            stacked=False,
            fill=False,
            label=label,
        )

        if plot_errors:
            axis_dict["left"]["top"].hist(
                x=bins[:-1],
                bins=bins,
                bottom=band,
                weights=unc * 2,
                label="stat. unc." if i == len(df_list) - 1 else None,
                fill=False,
                linewidth=0,
                hatch="/////",
                edgecolor="#666666",
            )

        bincounts.update({f"{i}": hist_counts})
        bincounts_unc.update({f"{i}": unc})

        # Start ratio plot
        if i != 0:
            # Calculate the step and step_unc for ratio
            step, step_unc = hist_ratio(
                nominator=bincounts[f"{i}"],
                denominator=bincounts["0"] if "0" in bincounts else bincounts[f"{i}"],
                nominator_unc=bincounts_unc[f"{i}"],
                denominator_unc=bincounts_unc["0"]
                if "0" in bincounts_unc
                else bincounts_unc[f"{i}"],
            )

            axis_dict["left"]["ratio"].step(
                x=Binning,
                y=step,
                color=f"C{i}",
                linestyle=linestyles[i],
            )

            if plot_errors:
                axis_dict["left"]["ratio"].fill_between(
                    x=Binning,
                    y1=step - step_unc,
                    y2=step + step_unc,
                    step="pre",
                    facecolor="none",
                    edgecolor="#666666",
                    linewidth=0,
                    hatch="/////",
                )

    # Add axes, titels and the legend
    axis_dict["left"]["top"].set_ylabel(
        "Normalised Number of Jets",
        fontsize=labelFontSize,
        horizontalalignment="right",
        y=1.0,
        color=ycolor,
    )
    if title is not None:
        axis_dict["left"]["top"].set_title(title)
    axis_dict["left"]["top"].tick_params(axis="y", labelcolor=ycolor)
    axis_dict["left"]["ratio"].set_xlabel(
        f"{variable}",
        horizontalalignment="right",
        x=1.0,
    )

    axis_dict["left"]["ratio"].set_ylabel(
        "Ratio",
        labelpad=labelpad,
        fontsize=labelFontSize,
    )

    if Ratio_Cut is not None:
        axis_dict["left"]["ratio"].set_ylim(bottom=Ratio_Cut[0], top=Ratio_Cut[1])

    plt.setp(axis_dict["left"]["top"].get_xticklabels(), visible=False)

    if xmin is not None:
        axis_dict["left"]["top"].set_xlim(left=xmin)

    else:
        axis_dict["left"]["top"].set_xlim(left=Binning[0])

    if xmax is not None:
        axis_dict["left"]["top"].set_xlim(right=xmax)

    else:
        axis_dict["left"]["top"].set_xlim(right=Binning[-1])

    if ymin is not None:
        axis_dict["left"]["top"].set_ylim(bottom=ymin)

    if ymax is not None:
        axis_dict["left"]["top"].set_ylim(top=ymax)

    # Add black line at one
    axis_dict["left"]["ratio"].axhline(
        y=1,
        xmin=0,
        xmax=1,
        color="black",
        alpha=0.5,
    )

    left_y_limits = axis_dict["left"]["top"].get_ylim()
    axis_dict["left"]["top"].set_ylim(
        left_y_limits[0], left_y_limits[1] * yAxisIncrease
    )

    axis_dict["left"]["top"].legend(
        loc=loc_legend,
        fontsize=legFontSize,
        ncol=ncol,
    )

    if UseAtlasTag is True:
        makeATLAStag(
            ax=axis_dict["left"]["top"],
            fig=fig,
            first_tag=AtlasTag,
            second_tag=SecondTag,
            ymax=yAxisAtlasTag,
        )

    savepath = os.path.join(plot_path, f"{variable}.{plot_type}")
    os.makedirs(plot_path, exist_ok=True)

    plt.tight_layout()
    plt.savefig(savepath, transparent=True, dpi=dpi)
    plt.close()

    if verbose:
        print("Done!")
        print()


def load_files(
    filepaths: str,
    filetype: str,
    mc_campaign: str = "mc16a",
    selection_type: str = "dilepton",
    tree: str = "nominal_Loose;1",
    weights: list = [
        "weight_mc",
        "weight_pileup",
        "weight_leptonSF",
        "weight_jvt",
        "weight_bTagSF_DL1r_Continuous",
    ],
):

    # Define years for the mc campaign
    if mc_campaign == "mc16a":
        year = ["2015", "2016"]

    elif mc_campaign == "mc16d":
        year = ["2017"]

    elif mc_campaign == "mc16e":
        year = ["2018"]

    else:
        raise ValueError(f"MC Campaign {mc_campaign} not supported")

    # Read the filepath of the specific category into glob
    filepath = glob(filepaths)

    # Init a dict for the loaded variables
    Return_dict = {}

    # Get number of files to load
    nFiles_to_load = len(filepath)

    # Check if files exist at given path
    if nFiles_to_load == 0:
        raise FileNotFoundError(f"No files could be found on given path {filepaths}")

    # Iterate over files
    for counter, file in enumerate(filepath):
        print(f"Opening {filetype} sample, file {counter+1} from {nFiles_to_load}...")

        # Check that only root files are loaded
        if file.endswith(".root"):

            # Open file with uproot
            with upt.open(file) as TTree:

                # Load the variables in a dict
                TTree_Dict = TTree[tree.encode()].arrays(TTree[tree.encode()].keys())

                # Decode the variable names
                TTree_Dict = {k.decode(): v for k, v in TTree_Dict.items()}

                # Calculate weights
                if weights:

                    # Get a array with ones for base
                    weights_array = np.ones_like(TTree_Dict[weights[0]])

                    # Combine the weights
                    for weight in weights:
                        weights_array = weights_array * TTree_Dict[weight]

                    # Add the combined weights to the dict
                    TTree_Dict["combined_weights"] = weights_array

                if selection_type == "dilepton":
                    nTaus_Cut = TTree_Dict["nTaus"] == 0

                    for year_counter in year:
                        if counter == 0:
                            emu_cut = (
                                (TTree_Dict[f"ee_{year_counter}_DL1r"] == 1)
                                | (TTree_Dict[f"emu_{year_counter}_DL1r"] == 1)
                                | (TTree_Dict[f"mumu_{year_counter}_DL1r"] == 1)
                            )

                        else:
                            emu_cut = (
                                emu_cut
                                | (TTree_Dict[f"ee_{year_counter}_DL1r"] == 1)
                                | (TTree_Dict[f"emu_{year_counter}_DL1r"] == 1)
                                | (TTree_Dict[f"mumu_{year_counter}_DL1r"] == 1)
                            )

                elif selection_type == "ljets":
                    nTaus_Cut = TTree_Dict["nTaus"] < 2

                    for year_counter in year:
                        if counter == 0:
                            emu_cut = (
                                TTree_Dict[f"ejets_{year_counter}_DL1r"] == 1
                            ) | (TTree_Dict[f"mujets_{year_counter}_DL1r"] == 1)

                        else:
                            emu_cut = (
                                emu_cut
                                | (TTree_Dict[f"ejets_{year_counter}_DL1r"] == 1)
                                | (TTree_Dict[f"mujets_{year_counter}_DL1r"] == 1)
                            )

                else:
                    raise ValueError(
                        f"Selection type {selection_type} is not supported!"
                    )

                # Get the mask
                Mask = nTaus_Cut & emu_cut

                # Apply mask to dict
                for key in TTree_Dict:
                    TTree_Dict[key] = TTree_Dict[key][Mask]

            # If this is the first file loaded, create new return dict
            if counter == 0:

                # Loop over variables in the dict
                for key in TTree_Dict:

                    # If jagged array (per-object variable), combine the flatted content
                    # Event info (to which event the object belongs will be lost)
                    # TODO Needs to be changed when you want to apply weights
                    if type(TTree_Dict[key]) == awk.array.jagged.JaggedArray:
                        Return_dict[key] = TTree_Dict[key].content

                    # If numpy array (per-event variable), combine.
                    elif type(TTree_Dict[key]) == np.ndarray:
                        Return_dict[key] = TTree_Dict[key]

                    # Print warning that a certain variable is not used
                    else:
                        print(f"Variable {key} will not be used!")

            # If this is not the first file loaded, append to existing dict
            else:

                # Loop over variables in the dict
                for key in TTree_Dict:

                    # If jagged array (per-object variable), combine the flatted content
                    # Event info (to which event the object belongs will be lost)
                    # TODO Needs to be changed when you want to apply weights
                    if type(TTree_Dict[key]) == awk.array.jagged.JaggedArray:
                        Return_dict[key] = np.append(
                            Return_dict[key], TTree_Dict[key].content, axis=0
                        )

                    # If numpy array (per-event variable), combine.
                    elif type(TTree_Dict[key]) == np.ndarray:
                        Return_dict[key] = np.append(
                            Return_dict[key], TTree_Dict[key], axis=0
                        )

                    # Print warning that a certain variable is not used
                    else:
                        print(f"Variable {key} will not be used!")

    return Return_dict


if __name__ == "__main__":
    Samples_Dir = "/work/ws/nemo/fr_af1100-Training-Simulations-0/ttH_Samples/"

    test_samples = load_files(
        filepaths=os.path.join(
            Samples_Dir,
            "test_samples",
            "user.aknue.mc16_13TeV.346344.PhPy8EG_ttH125_ljets.TOPQ1."
            "e7148s3126r9364p4514.TTHbb212197-v1_TEST3n_2l_out_root/",
            "*",
        ),
        filetype="Test",
    )

    control_samples = load_files(
        filepaths=os.path.join(
            Samples_Dir,
            "control_samples",
            "user.aknue.346344.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r9364_p4346"
            "._TTH_PFlow_212171_V2_dil_out_root/",
            "*",
        ),
        filetype="Control",
    )

    for variable, _ in test_samples.items():
        plot_ratio_hist(
            df_list=[control_samples, test_samples],
            variable=variable,
            plot_path="/home/fr/fr_fr/fr_af1100/ttH/control_plots/",
            label_list=["Control Sample", "Test Sample"],
            Ratio_Cut=[0.8, 1.2],
            plot_type="png",
        )
