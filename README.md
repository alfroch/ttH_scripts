# ttH_scripts
ttH scripts with a docker container for easy usage. Various machine learning programs are included in the container. This includes Tensorflow, Keras, Uproot etc.

## How to pull in Nemo
To pull the container and use it in Nemo, add the following lines to your .bashrc on Nemo:
```
UprootContainer()
{
    module load tools/singularity/3.8
    singularity exec --nv --contain --pwd PATHTOYOURHOME:PATHTOYOURHOME -B /work -B /home -B /tmp docker://gitlab-registry.cern.ch/alfroch/tth_scripts:latest bash
}
```

The `-B` option allows you to mount more directories to the container.

After the pulling and initalsing is done, you can call the container by typing ```UprootContainer```
